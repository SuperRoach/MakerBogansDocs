#Printer goop
This is a group buy for a PVA-beating print adhesive. 

## What is it?
This is a mixture you can spray or apply to your printer, with the idea that it will make it stick very well, until you don't need it anymore. Typically this means your printed bed must cool down before being able to remove it.

## Where to get it.

## How to use it:
PVP/VA co-polymer powder, active ingredient in hair spray. Mix with water, or a water/alcohol mix in 2-5% by weight. Spray on plate. Redistribute on plate with standard IPA wipe after you lift prints. Mostly invisible on prints, but washes off with water. Group buy is an envelope with a coin bag stuffed with the raw powder. This is basically Dimafix and other commercial adhesives but the raw powder is enough for something like 20 dimafix pens or more (which they sell for $40 a throw). 

## How can I get a smaller quantity for a single person?
Please see #flea-market on the discord.

## What's a good thing to put it in?
A small spray bottle is good. Also a marker pen as a more direct and light way to apply it. A typical example of a small spray bottle is the kind you get with glasses to clean them. 
https://www.diffusional.com.au/assets/full/SB-G-MB-50-BL.jpg?20200711030705 (example 50ml spray bottle)